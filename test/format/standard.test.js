
import params from "../../src/format/standard";

import paramsTester from "./paramstester";

describe("Standard Format Parameters", paramsTester(params));
