
import params from "../../src/format/immutable";

import paramsTester from "./paramstester";

describe("Immutable.js Format Parameters", paramsTester(params));
