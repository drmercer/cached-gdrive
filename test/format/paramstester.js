import assert from "assert";
import DEFAULT_PARAMS from "../../src/format/defaults";

export default partialParams => () => {

	const params = Object.assign({}, DEFAULT_PARAMS, partialParams);
	const A = {
		a: {b: 1},
		c: 2,
	};
	const B = {
		a: {b: 2},
		c: 2,
		potato: "This is a potato",
	};

	describe("diff", () => {

		it("should return a falsey value for identical objects", () => {

			const a = params.fromJson(clone(A));
			const a2 = params.fromJson(clone(A));

			const diff = params.diff(a, a2);
			assert(!diff);

		});

		it("should return a JSON-serializable object", () => {

			const a = params.fromJson(clone(A));
			const b = params.fromJson(clone(B));

			const diff = params.diff(a, b);
			// Assert it's serializable
			assert.deepStrictEqual(diff, JSON.parse(JSON.stringify((diff))));

		});

	});

	describe("patch", () => {

		it("should apply a patch to lhs and obtain an identical object to rhs", () => {
			const a = params.fromJson(clone(A));
			const b = params.fromJson(clone(B));

			const diff = params.diff(a, b);
			const c = params.patch(a, diff);
			assert.deepStrictEqual(b, c);
		});

		it("should not mutate the before object when applying patch", () => {
			const a = params.fromJson(clone(A));
			const aOrig = params.fromJson(clone(A));
			const b = params.fromJson(clone(B));

			const diff = params.diff(a, b);
			params.patch(a, diff);

			assert.deepStrictEqual(a, aOrig);
		});

	});

	describe("mergeContents", () => {

		it("should prefer server changes over client changes", () => {
			const cached = params.fromJson({
				a: {b: 1},
				c: 2,
			});
			const changed = params.fromJson({
				a: {b: 2},
				potato: "This is a potato",
			});
			const server = params.fromJson({
				a: {b: "bagel"},
			});

			const localChange = params.diff(cached, changed);
			const { mergedContents, serverHadChanges } = params.mergeContents(server, cached, localChange);
			assert.deepStrictEqual(mergedContents, params.fromJson({
				a: {b: "bagel"},
				potato: "This is a potato",
			}));
			assert.strictEqual(serverHadChanges, true, "server had changes");
		});

		it("should properly merge server changes when no client changes", () => {
			const cached = params.fromJson({
				a: {b: 1},
				c: 2,
			});
			const server = params.fromJson({
				a: {b: "bagel"},
			});

			const localChange = null;
			const { mergedContents, serverHadChanges } = params.mergeContents(server, cached, localChange);
			assert.deepStrictEqual(mergedContents, params.fromJson({
				a: {b: "bagel"},
			}));
			assert.strictEqual(serverHadChanges, true, "server had changes");
		});

		it("should properly merge when no changes in serverContents", () => {
			const cached = params.fromJson({
				a: {b: 1},
				c: 2,
			});
			const changed = params.fromJson({
				a: {b: 2},
				potato: "This is a potato",
			});
			const server = cached;

			const localChange = params.diff(cached, changed);
			const { mergedContents, serverHadChanges } = params.mergeContents(server, cached, localChange);
			assert.deepStrictEqual(mergedContents, changed);
			assert.strictEqual(serverHadChanges, false, "server had no changes");
		});

		it("should properly merge when no serverContents", () => {
			const cached = params.fromJson({
				a: {b: 1},
				c: 2,
			});
			const changed = params.fromJson({
				a: {b: 2},
				potato: "This is a potato",
			});
			const server = null;

			const localChange = params.diff(cached, changed);
			const { mergedContents, serverHadChanges } = params.mergeContents(server, cached, localChange);
			assert.deepStrictEqual(mergedContents, changed);
			assert.strictEqual(serverHadChanges, false, "server had no changes");
		});

	});
};

function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}
