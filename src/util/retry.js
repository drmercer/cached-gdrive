import promiseRetry from "promise-retry";

const OPTIONS = {
	retries: 4,
	factor: 1.6,
	minTimeout: 800,
};

const LOG_RETRIES = false;
const ALWAYS_RETRY = ()=>true;

export default function retry(fn, {shouldRetry=ALWAYS_RETRY}={}) {
	const start = Promise.resolve();
	let retried = false;
	return promiseRetry(
		// Safely call fn, and retry if an error happens
		tryAgain => start.then(fn).catch(err => {
			retried = true;
			if (LOG_RETRIES) {
				console.log("%cMaybe retrying...", "color:salmon", err);
			}
			if (shouldRetry(err)) {
				tryAgain(err);
			} else {
				throw err;
			}
		}),
		OPTIONS
	).then(result => {
		if (retried && LOG_RETRIES) {
			console.log("%cRetried operation and succeeded. :)", "color:lightgreen");
		}
		return result;
	}, err => {
		if (LOG_RETRIES) {
			console.log("%cRetried operation, but failed. :(", "color:salmon", err);
		}
		throw err;
	});
}

export function retryInline(fn) {
	return param => retry(fn, param);
}
