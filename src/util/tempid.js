import shortid from "shortid";

export default {
	create,
	test,
	scrapeFrom,
};

function create() {
	return "$temp$" + shortid.generate();
}

function test(id) {
	return id.startsWith("$temp$");
}

const scrapeRegex = new RegExp("\\$temp\\$[A-Za-z0-9_-]+");
function scrapeFrom(str) {
	const match = str.match(scrapeRegex);
	return match ? match[0] : null;
}
