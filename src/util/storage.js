/*
 * An abstracted wrapper of a client-side storage implementation. Currently uses
 * localStorage. Could be changed to use some other storage solution, but it
 * must be synchronous (to avoid race conditions when syncing and changing a
 * file's contents "simultaneously").
 */

const PREFIX = "cgd:";
const prefix = key => PREFIX + key;
const hasPrefix = key => key.startsWith(PREFIX);
const unprefix = key => hasPrefix(key) ? key.slice(PREFIX.length) : key;

const isQuotaExceededErr = err =>
	err && err.code && (err.code === 22 || err.code === 1014);


function set(key, value) {
	if (typeof value === "undefined") {
		remove(key);
	} else {
		_logAccessTo(key);
		try {
			localStorage.setItem(prefix(key), JSON.stringify(value));
		} catch (err) {
			if (!isQuotaExceededErr(err)) throw err;
			// Prune and try again:
			prune();
			set(key, value);
		}
	}
}

function get(key) {
	_logAccessTo(key);
	return JSON.parse(localStorage.getItem(prefix(key)));
}

function move(oldKey, newKey) {
	const oldKeyPrefixed = prefix(oldKey);
	const value = localStorage.getItem(oldKeyPrefixed);
	localStorage.removeItem(oldKeyPrefixed);
	localStorage.setItem(prefix(newKey), value);
	_logAccessTo(newKey);
	_removeAccessLog(oldKey);
}

function remove(key) {
	_removeAccessLog(key);
	localStorage.removeItem(prefix(key));
}

function keys() {
	const count = localStorage.length;
	const list = [];
	for (let i = 0; i < count; i++) {
		const key = localStorage.key(i);
		if (hasPrefix(key)) {
			list.push(unprefix(key));
		}
	}
	return list;
}

function clear() {
	const keysToRemove = keys();
	keysToRemove.forEach(remove);
}

//======================================================================
//			Pruning

// Max age of 10 days
const MAX_AGE_MILLIS = 1000*60*60*24*10;
// Prefix for timestamp entries:
const TIMESTAMP_PREFIX = "lastAccess:";

var filterRegex = null;
function setPruningFilter(re) {
	filterRegex = re;
}

const _shouldTrack = key =>
	filterRegex
	&& !key.startsWith(TIMESTAMP_PREFIX)
	&& filterRegex.test(key);

function _logAccessTo(key) {
	if (_shouldTrack(key))
		set(TIMESTAMP_PREFIX+key, Date.now());
}

function _getLastAccessTime(key) {
	return +get(TIMESTAMP_PREFIX+key);
}

function _removeAccessLog(key) {
	if (_shouldTrack(key))
		remove(TIMESTAMP_PREFIX+key);
}

function prune() {
	const allKeys = keys();
	allKeys.forEach(key => {
		if (_shouldTrack(key) &&
				_getLastAccessTime(key) < Date.now() - MAX_AGE_MILLIS) {
			// Key is stale, delete it:
			remove(key);
		}
	});
}

//======================================================================

export default { get, set, move, remove, keys, clear, setPruningFilter };
