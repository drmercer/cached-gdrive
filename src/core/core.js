import { gdsFactory } from "gdrive-simple";

import CachedFile from "./cachedfile";
import CachedFolder from "./cachedfolder";
import ContentsHelper from "./contentshelper";
import Utils from "./utils";
import SyncTimer from "./synctimer";

// This is called with the diff, patch, etc. options (e.g. from standard.js)
export default (params) => {

	const contentsHelper = new ContentsHelper(params);
	const utils = new Utils(CachedFile, CachedFolder, contentsHelper);
	const connection = utils.connection;
	const cachedGDrive = gdsFactory(CachedFile, CachedFolder, utils);

	//======================================================================
	//			Initializing

	// Give original init method to the ConnectionUtils
	connection.initClient = cachedGDrive.init;
	// Make a new init method so we can only initialize when needed.
	cachedGDrive.init = params => {
		// This method will try to init if we're online:
		connection.setClientInitParams(params);
		return Promise.resolve();
	};

	//======================================================================
	//			Connection-watching

	// The dev needs to listen for connection change instead of just signed-in change.
	cachedGDrive.listenForSignInChange = () => {
		throw new Error(
			"Do not call listenForSignInChange when using cached-gdrive. "+
			"Use watchConnection instead.");
	};
	cachedGDrive.watchConnection = cb => {
		utils.events.on("connectionStatusChange", cb);
		// Fire listener with current status, if there is one
		const currentStatus = utils.connection.status;
		if (currentStatus) cb(currentStatus);
	};

	//======================================================================
	//			Sync

	const syncTimer = new SyncTimer(utils);

	cachedGDrive.requestSync = syncTimer.requestSync.bind(syncTimer);
	cachedGDrive.setSyncInterval = interval => syncTimer.syncInterval = interval;

	//======================================================================
	//			User profile

	/**
	 * @return {Object} A plain JS object describing the current user profile.
	 */
	cachedGDrive.getCachedUserProfile = () => utils.connection.getUserProfile();
	cachedGDrive.getUserProfile = () => {
		throw new Error(
			"Do not call getUserProfile when using cached-gdrive. "+
			"Use getCachedUserProfile instead.");
	};

	//======================================================================
	//			Sign in/out

	// The original gds.signOut function (signs out from Google)
	const gapiSignOut = cachedGDrive.signOut;
	// Clears the cache, then signs out from Google
	const deleteCacheAndSignOut = () => {
		utils.cache.clearCache();
		// TODO: remove all changeContents: event listeners somehow?
		return gapiSignOut();
	};
	// Safe signOut function (requires a force flag before deleting unsynced user data):
	const trySignOut = ({force}={}) => {
		if (connection.isUsable) {
			// If connected, sync the cache and then sign out. If force=true, ignore errors in sync.
			return syncTimer.requestSync(true)
			.then(res => {
				// Handle partially successful sync
				if (!res.allSynced) throw res;
			}, err => {
				// Log failed sync
				console.error("Error in requested sync:", err);
				throw err;
			})
			.then(
				deleteCacheAndSignOut,
			err => {
				if (force) {
					deleteCacheAndSignOut();
				} else {
					// Set userWouldLoseOfflineChanges flag on error
					throw Object.assign(err || {}, mustForceSignOut);
				}
			});
		} else {
			// If not connected, only sign out if force flag is true
			return force ? deleteCacheAndSignOut() : Promise.reject(mustForceSignOut);
		}
	};

	cachedGDrive.signOut = (options={}) => {
		const {force, promptForForce, reload} = options;
		return trySignOut({force})
			.then(() => {
				if (reload) window.location.reload();
			}, err => {
				if (err
						&& err.userWouldLoseOfflineChanges
						&& promptForForce
						&& confirm("You will lose any data that hasn't synced yet. Sign out?")) {
					// Call again with force=true
					options.force = true;
					return cachedGDrive.signOut(options);
				} else {
					throw err;
				}
			});
	};

	// Error flag to signify danger of data loss:
	const mustForceSignOut = { userWouldLoseOfflineChanges:true };

	//======================================================================
	//			"Wrong user" resolution

	cachedGDrive.resolveWrongUser = keep => {
		if (keep) {
			// Clear local data
			utils.cache.clearCache();
			connection.updateConnectionStatus();
		} else {
			// Sign out, then sign in again
			gapiSignOut()
				.then(() => cachedGDrive.signIn({prompt:"select_account"}));
		}
	};


	return cachedGDrive;
};
