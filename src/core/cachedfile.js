import { File } from "gdrive-simple";
import tempid from "../util/tempid";

export default class CachedFile extends File {

	getDataToCache() {
		return {
			id: this.id,
			spaces: this.spaces,
			name: this.name,
			isFolder: false,
		};
	}

	//======================================================================
	//			Write

	write(data) {
		this.id = this.utils.updateIdIfNecessary(this.id);
		this.utils.cache.updateCachedContents(this.id, data);
		// TODO: if there were no changes compared to the cache (and if user is
		// connected), return a promise for a sync.
		return Promise.resolve({ savedOffline: true });
	}

	_safeWrite(data) {
		this.utils.network.writeFileContentsFormatted(this.id, data);
	}

	/**
	 * Initializes the file's contents cache with the given contents, unless the
	 * cache already has contents for this file, in which case this does nothing.
	 * @param {Formatted contents} defaultContents The base contents of the file.
	 * @return {CachedFile} `this` CachedFile for chaining.
	 */
	setDefaultContents(defaultContents) {
		this.id = this.utils.updateIdIfNecessary(this.id);
		try {
			this.utils.cache.cacheContents(this.id, defaultContents);
		} catch (err) {
			// `cacheNotEmpty` flag means some contents are already in the cache. We
			// don't care, since we just want to make sure *something* is in the cache
			if (!err.cacheNotEmpty) throw err;
		}
		return this;
	}

	//======================================================================
	//			Read

	async read() {
		this.id = this.utils.updateIdIfNecessary(this.id);
		// Attempt to get contents from cache:
		var contents = this.utils.cache.getCachedContents(this.id);
		if (contents) return contents;

		if (this.utils.connection.isUsable) {
			// User is connected

			if (!tempid.test(this.id)) {
				// GET file contents
				contents = await this._safeRead();

				if (contents) {
					// Cache and return retrieved contents
					this.utils.cache.cacheContents(this.id, contents);
					return contents;

				} else {
					// No contents
					return null;
				}

			} else {
				// TODO: Maybe do sync, then try again?
				throw new Error("You cannot read from a temp file before sync without specifying a defaultContents");
			}

		} else {
			const err = new Error("The user must be online to read from a file with no defaultContents!");
			err.mustBeOnline = true;
			throw err;
		}
	}

	_safeRead() {
		return this.utils.network.readFileContentsFormatted(this.id);
	}

	//======================================================================
	//			Watching

	watch(listener) {
		this.read()
			.then(contents => {
				listener(contents);
				return contents;
			});
		this.utils.events.on("changeContents:"+this.id, listener);
	}

	unwatch() {
		this.id = this.utils.updateIdIfNecessary(this.id);
		this.utils.events.removeAllListeners("changeContents:"+this.id);
	}

	//======================================================================

	hasTempId() {
		this.id = this.utils.updateIdIfNecessary(this.id);
		return tempid.test(this.id);
	}
}
