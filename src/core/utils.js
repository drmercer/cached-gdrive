import { Utils as BaseUtils } from "gdrive-simple";
import EventEmitter from "eventemitter3";

import NetworkUtils from "./networkutils";
import CacheUtils from "./cacheutils";
import ConnectionUtils from "./connectionutils";
import tempid from "../util/tempid";

/**
 * The special subclass of gdrive-simple's Utils class. Contains utilities for
 * managing the cache and the network.
 */
export default class Utils extends BaseUtils {
	constructor(File, Folder, contentsHelper) {
		super(File, Folder);
		this._contentsHelper = contentsHelper;
		this.events = new EventEmitter();
		// Override network property of superclass:
		this.network = new NetworkUtils(contentsHelper);
		this.connection = new ConnectionUtils(this.events);
		this.network.onReloadNeeded = () => this.connection.handleReloadNeeded();
		this.cache = new CacheUtils(contentsHelper);

		this._resolvedIds = {};
	}

	updateIdIfNecessary(id) {
		return this._resolvedIds[id] || id;
	}

	//======================================================================
	//			Sync

	async sync() {
		console.time("Sync");

		// Get list of temporary files:
		var tempFileRecords = this.cache.getTempItemRecords();
		const failures = [];
		const resolvedIds = {};
		// Try to get actual file data for each record:
		for (let i = 0; i < tempFileRecords.length; i++)  {
			const record = tempFileRecords[i];
			// Resolve the parent ID if needed:
			if (resolvedIds[record.parent]) {
				record.parent = resolvedIds[record.parent];
			}
			// Get the actual file ID:
			try {
				const data = await this.network
					.getOrCreateFile(record.parent, record.spaces, record.name, record.isFolder);
				// Add temp ID and real ID to map
				resolvedIds[record.id] = data.id;
			} catch (err) {
				// Add record to failures list
				failures.push(record);
			}
		}
		// Put any failures back into storage, to try again later (TODO: something better to do here?):
		this.cache.restoreTempItemRecords(failures);

		// Update anything in localStorage that uses a tempId to use the real ID instead.
		this.cache.replaceTempIds(resolvedIds);

		// Update any event listeners
		for (const tempId in resolvedIds) {
			const realId = resolvedIds[tempId];
			const listeners = this.events.listeners("changeContents:"+tempId);
			// NOTE: because this doesn't transfer contexts, we cannot use contexts
			// with these event callbacks.
			this.events.removeAllListeners("changeContents:"+tempId);
			listeners.forEach(l => {
				this.events.on("changeContents:"+realId, l);
			});
		}

		// Update the persistent resolved IDs map
		Object.assign(this._resolvedIds, resolvedIds);

		// Get ids of files with cached contents:
		const cachedFileIds = this.cache.getAllCachedContentFileIds()
			// filter out files with temp. IDs - ignore them until they're resolved
			.filter(id => !tempid.test(id));

		// Sync each file with local changes:
		const syncPromises = cachedFileIds.map(id => this._syncFile(id));
		const results = await Promise.all(syncPromises);
		const allSuccessful = results.every(res => res.success);
		console.timeEnd("Sync");
		return { allSynced: allSuccessful };
	}

	async _syncFile(id) {
		const gettingServerContents = this.network
				.readFileContentsFormatted(id);

		const {baseContents: cachedContents, changes: changes}
				= this.cache.getCachedContents(id, {keepSeparate:true});
		const weHaveChanges = !!changes;
		const serverContents = await gettingServerContents;

		// ~ Now both versions and the local changes have been obtained ~

		const { mergedContents, serverHadChanges }
			= this._contentsHelper.mergeContents(serverContents, cachedContents, changes);

		// Do nothing if nothing has changed on either side:
		if (!serverHadChanges && !weHaveChanges) return { success: true };

		try {
			if (weHaveChanges) {
				// Write to server:
				await this.network.writeFileContentsFormatted(id, mergedContents);
			}
			// Save finalContents to cache:
			this.cache.cacheContents(id, mergedContents, true);
			if (serverHadChanges) {
				// Notify interested parties that the file's contents have been updated:
				this.events.emit("changeContents:"+id, mergedContents);
			}
			return { success: true };

		} catch (err) {
			console.error("Error writing merged contents to server:", err);
			// Do not change local cache, because write to server failed.

			return { success: false };
		}

	}
}
