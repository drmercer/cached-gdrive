// The default parameters
import DEFAULT_PARAMS from "../format/defaults";

export default class ContentsHelper {
	constructor(params) {
		Object.assign(this, DEFAULT_PARAMS, params);
	}

	cloneContents(notraw) {
		return this.fromJson(
			this._cloneRawContents(
				this.toJson(notraw)));
	}

	_cloneRawContents(raw) {
		return JSON.parse(JSON.stringify(raw));
	}

	/**
	 * Called when passing contents data to/from the developer, so that we can
	 * prevent mutation of cached copies if necessary.
	 * @param  {Formatted contents} contents The file contents to clone if needed.
	 * @return {Formatted contents} The cloned contents, or the reference passed in.
	 */
	cloneIfNecessary(contents) {
		// Clone the contents if willMutateOutput.
		return this.willMutateOutput ? this.cloneContents(contents) : contents;
	}

	//======================================================================
	//			Default functions (can be overridden by params)

	toJson(notraw) {
		return notraw;
	}

	fromJson(raw) {
		return raw;
	}

	diff() {
		throw new Error("Must specify a diff(before, after) function when initializing cached-gdrive");
	}

	patch() {
		throw new Error("Must specify a patch(before, changes) function when initializing cached-gdrive");
	}
}
