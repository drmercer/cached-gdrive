
const SYNC_INTERVAL = 4000;

export default class SyncTimer {
	constructor(utils) {
		this.syncInterval = SYNC_INTERVAL;
		// Keep pointers to Utils and its events handler
		this.utils = utils;
		utils.events.on("connectionStatusChange", this._handleConnectionStatusChange, this);
	}

	_handleConnectionStatusChange() {
		const isConnectionUsable = this.utils.connection.isUsable;
		if (isConnectionUsable === this.isSyncingRegularly) return;

		if (isConnectionUsable) {
			this._startRegularSync();
		} else {
			this._stopRegularSync();
		}
	}

	_startRegularSync() {
		this.isSyncingRegularly = true;
		return this._doSync();
	}

	// Starts a sync, after which it always calls _scheduleNextSync()
	_doSync() {
		const syncing = this.utils.sync();
		this.syncInProgress = syncing;

		syncing.then(result => {
			if (!result.allSynced) {
				console.warn("Not all files were synced.");
			}
			this.syncInProgress = null;
			this._scheduleNextSync();
			return result;
		}, err => {
			this.syncInProgress = null;
			// Even if an error occurs, schedule the next sync
			this._scheduleNextSync();
			console.error("error in sync:", err);
		});

		return syncing;
	}

	// Schedules the next sync, unless sync has been stopped
	_scheduleNextSync() {
		if (this.isSyncingRegularly) {
			this.nextSyncTimeoutId = setTimeout(() => this._doSync(), this.syncInterval);
		}
	}

	_stopRegularSync() {
		clearTimeout(this.nextSyncTimeoutId);
		this.nextSyncTimeoutId = -1; // Clear the ID
		this.isSyncingRegularly = false;
	}

	/**
	 * Stops and restarts the sync cycle immediately, or returns a Promise for the
	 * currently-executing sync, if there is one.
	 * @param  {Boolean} forceNew
	 *         (optional) If true, will not return a Promise for the currently-
	 *         executing sync, if there is one, and instead will return a Promise
	 *         for a sync immediately after that currently-executing sync.
	 *         Defaults to false.
	 * @return {Promise}
	 *         The Promise of the freshly-executed sync's result.
	 */
	requestSync(forceNew=false) {
		// If a sync is currently in progress, return its Promise instead of starting a new one:
		if (this.syncInProgress && !forceNew)
			return this.syncInProgress;
		// TODO: check if connection is usable?
		this._stopRegularSync();
		// Restart the sync cycle after the call stack clears and any current sync is done:
		return Promise
			.resolve(this.syncInProgress)
			.catch(NO_OP) // swallow errors in previous sync silently
			.then(() => this._startRegularSync());
	}
}

const NO_OP = () => {};
