/* global gapi */

import storage from "../util/storage";
import retry from "../util/retry.js";

export default class ConnectionUtils {
	constructor(events) {
		// A reference to utils.events
		this.events = events;
		this.status = null;
		this._resetInitFlags();

		window.addEventListener("online", this.handleNetworkConnected.bind(this));
		window.addEventListener("offline", this.handleNetworkDisconnected.bind(this));
	}

	// Called near the beginning of the program
	setClientInitParams(params) {
		this._initParams = params;
		// Handle the current network state (since we're starting up the app)
		if (navigator.onLine) {
			this.handleNetworkConnected();
		} else {
			this.handleNetworkDisconnected();
		}
	}

	handleReloadNeeded() {
		this._reloadNeeded = true;
		this.updateConnectionStatus();
	}

	handleNetworkConnected() {
		console.log("%cOnline", "color: chartreuse");
		if (this._initParams) {
			this._tryToInitClient();
		}
	}

	handleNetworkDisconnected() {
		console.log("%cOffline", "color: salmon");
		this._resetInitFlags();
		this.updateConnectionStatus();
	}

	_resetInitFlags() {
		this._initialized = false;
	}

	_tryToInitClient() {
		this._resetInitFlags();
		// Initialize the client with the given parameters
		this.clientInitializing = retry(
				() => this.initClient(this._initParams)
			)
			.then(
				() => {
					this._initialized = true;
					this._watchSignedInState();
					this.updateConnectionStatus();
				},
				() => {
					console.warn("Error initializing gapi client");
					this.updateConnectionStatus();
				}
			);
	}

	initClient(/*params*/) {/* Abstract. Defined in core.js */}

	_watchSignedInState() {
		if (!this._isWatchingSignIn) {
			gapi.auth2.getAuthInstance().isSignedIn.listen(() => {
				this.updateConnectionStatus();
			});
			this._isWatchingSignIn = true;
		}
	}

	getConnectionStatus() {
		const status = {
			isConnected: navigator.onLine && this._initialized,
		};
		if (this._reloadNeeded) {
			status.reloadNeeded = true;
		}
		var userProfile = null;
		if (status.isConnected && !this._reloadNeeded) {
			const auth = gapi.auth2.getAuthInstance();
			if (auth.isSignedIn.get()) {
				// Signed in, get current user:
				const user = auth.currentUser.get().getBasicProfile();
				userProfile = {
					id: user.getId(),
					name: user.getName(),
					givenName: user.getGivenName(),
					familyName: user.getFamilyName(),
					imageUrl: user.getImageUrl(),
					email: user.getEmail(),
				};
			}
		}
		// Validate user, then cache and return status:
		this._validateUser(userProfile, status);
		status.isUsable = isStatusUsable(status);
		this.status = status;
		return status;
	}

	/**
	 * Fires a "connectionStatusChange" event with the current connection status
	 * (after the current call stack clears).
	 */
	updateConnectionStatus() {
		Promise
			.resolve(this.getConnectionStatus())
			.then(status => this.events.emit("connectionStatusChange", status));
	}

	/**
	 * `true` if Google Drive read/write operations can successfully occur.
	 * @return {Boolean} [description]
	 */
	get isUsable() {
		return this.status.isUsable;
	}

	/**
	 * Makes sure that the signed-in user (the parameter) matches the user data
	 * of the cache.
	 * @param  {Object} userProfile
	 *           A hash of properties of the current user.
	 * @param  {Object} status
	 *           A hash of flags representing the current status. This is mutated
	 *           to reflect the results of the validation.
	 * @return {Promise}
	 *           A Promise that resolves when the validation is complete.
	 */
	_validateUser(userProfile, status) {
		const userId = userProfile && userProfile.id;

		const cachedProfile = storage.get("user-profile");
		// Compare cached ID to current user ID:
		if (cachedProfile) {
			// Was previously signed in:
			status.isSignedIn = true;
			const cachedId = cachedProfile.id;
			if (!userId) {
				// Not signed in with Google (but "signed in" in the cache)
				if (status.isConnected) status.needsToAuthenticate = true;
			} else if (userId !== cachedId) {
				// Signed in with Google, but with the wrong account!
				status.wrongUser = {
					expected: cachedProfile,
					actual: userProfile,
				};
			}
		} else {
			// Not previously signed in
			status.isSignedIn = !!userId;
		}
		// Cache the new profile (in case imageUrl, email, etc. changed):
		if (userProfile) {
			storage.set("user-profile", userProfile);
		}
	}

	getUserProfile() {
		return storage.get("user-profile");
	}
}

function isStatusUsable(status) {
	return status.isConnected
		&& !status.reloadNeeded
		&& status.isSignedIn
		&& !status.needsToAuthenticate
		&& !status.wrongUser;
}
