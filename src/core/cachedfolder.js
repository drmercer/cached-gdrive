import { Folder } from "gdrive-simple";

export default class CachedFolder extends Folder {

	getDataToCache() {
		return {
			id: this.id,
			spaces: this.spaces,
			name: this.name,
			isFolder: true,
		};
	}

	listFiles(options={}) {
		const {
			preferCache=false,
			folders=false,
			numToCache=0,
		} = options;

		this.id = this.utils.updateIdIfNecessary(this.id);

		if (this.utils.connection.isUsable && !preferCache) {

			// Load from network
			return super.listFiles(options).then(result => {
				// cache the data of <numToCache> files
				result.files.slice(0, numToCache).forEach(file => {
					this.utils.cache.cacheItem(file.getDataToCache(), this.id);
				});
				return result;
			});

		} else {

			// Load from cache
			const filesData = this.utils.cache.listCachedItems(this.id, folders);
			const files = filesData.map(data => this.utils.inflateData(data, folders));
			return Promise.resolve( {files, incomplete: true} );

		}
	}

	getFile(name, options={}) {
		const { folder=false, preferNetwork=false } = options;
		this.id = this.utils.updateIdIfNecessary(this.id);

		// Try to get the file from cache:
		const data = this.utils.cache.getCachedItem(name, this.id);
		if (data) {
			// Return file from cache:
			return Promise.resolve(this.utils.inflateData(data, folder));

		} else if (preferNetwork && this.utils.connection.isUsable) {
			// Get file from drive:
			return super.getFile(name, options)
					.then(file => {
						this.utils.cache.cacheItem(file.getDataToCache(), this.id);
						return file;
					});

		} else {
			// Get a temporary file:
			const tempData = this.utils.cache.getTempCachedItem(this.id, this.spaces, name, folder);
			const file = this.utils.inflateData(tempData, folder);
			return Promise.resolve(file);
		}
	}
}
