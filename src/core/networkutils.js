import { NetworkUtils as BaseNetworkUtils } from "gdrive-simple";
import retry from "../util/retry.js";

/**
 * Wraps the methods of gdrive-simple's NetworkUtils with the retry utility for
 * better safety.
 */
export default class NetworkUtils extends BaseNetworkUtils {
	constructor(contentsHelper) {
		super();
		this._contentsHelper = contentsHelper;
	}

	getOrCreateFile(...args) {
		return retry(() => super.getOrCreateFile(...args),
			{ shouldRetry: e => this._shouldRetry(e) });
	}

	listFolderChildren(...args) {
		return retry(() => super.listFolderChildren(...args),
			{ shouldRetry: e => this._shouldRetry(e) });
	}

	readFileContents(...args) {
		return retry(() => super.readFileContents(...args),
			{ shouldRetry: e => this._shouldRetry(e) });
	}

	readFileContentsFormatted(...args) {
		return this.readFileContents(...args)
			.then(this._contentsHelper.fromJson);
	}

	writeFileContents(id, contents) {
		return retry(() => super.writeFileContents(id, contents),
			{ shouldRetry: e => this._shouldRetry(e) });
	}

	writeFileContentsFormatted(id, contents) {
		const rawContents = this._contentsHelper.toJson(contents);
		return this.writeFileContents(id, rawContents);
	}

	// Called by retry() when an error happens. Should react to errors where
	// retrying won't help (and then return false to forego retrying).
	_shouldRetry(error) {
		if (error.status === 401) {
			// Handle Authorization error by suggesting user should reload page
			this.onReloadNeeded();
			return false;
		} else if (error.status === 403 && error.result.error.message.includes("User Rate Limit")) {
			console.warn("User Rate Limit error! Consider turning down the sync frequency using .setSyncInterval().");
		}
		return true;
	}

	onReloadNeeded() {/* Abstract. Defined in Utils */}
}
