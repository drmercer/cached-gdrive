import storage from "../util/storage";
import tempid from "../util/tempid";

export default class CacheUtils {

	constructor(contentsHelper) {
		this._contentsHelper = contentsHelper;
		this._memoryCache = {};
	}

	clearCache() {
		storage.clear();
		this._memoryCache = {};
	}

	//======================================================================
	//			Item (file/folder) metadata

	getCachedItem(fileName, parentId) {
		const cacheKey = getItemDataKey(parentId, fileName);
		const data = storage.get(cacheKey);
		return data;
	}

	cacheItem(item, parentId) {
		const cacheKey = getItemDataKey(parentId, item.name);

		try {
			storage.set(cacheKey, item);
		} catch (err) {
			console.warn("Caching failed.", err);
		}
	}

	listCachedItems(parentId, folders) {
		storage.keys()
			.filter(isItemDataKey)
			.filter(key => key.includes(parentId))
			.map(key => storage.get(key))
			.filter(data => data.isFolder === folders);
	}

	//---------------------------------------------------
	//			Temporary item data (e.g. when ID cannot be determined)

	getTempCachedItem(parentId, spaces, name, isFolder) {
		const id = tempid.create();

		const tempRecord = {
			parent: parentId,
			spaces: spaces,
			name: name,
			id: id,
			isFolder: isFolder,
		};

		this._appendTempItemRecord(tempRecord);

		const item = { id, name, spaces };
		this.cacheItem(item, parentId);
		return item;
	}

	_appendTempItemRecord(tempRecord) {
		const records = this.getTempItemRecords();
		records.push(tempRecord);
		storage.set(TEMP_ITEM_RECORDS_KEY, records);
	}

	/**
	 * Gets, clears, and returns the temp item records array
	 * @return {Array} An array of records
	 */
	getTempItemRecords() {
		const records = storage.get(TEMP_ITEM_RECORDS_KEY) || [];
		storage.set(TEMP_ITEM_RECORDS_KEY, []);
		return records;
	}

	/**
	 * Prepends the given oldRecords onto the stored records array.
	 * @param  {Array}  [oldRecords=[]] The records to re-store.
	 */
	restoreTempItemRecords(oldRecords=[]) {
		const records = this.getTempItemRecords();
		storage.set(TEMP_ITEM_RECORDS_KEY, oldRecords.concat(records));
	}

	/**
	 * Replaces any references to temp IDs (the keys of the map) with the actual
	 * IDs (the values in the map).
	 * @param  {Object} resolvedIds A map of temp IDs to real IDs.
	 */
	replaceTempIds(resolvedIds) {
		// Replace in keys
		const keys = storage.keys();
		keys.forEach(key => {
			const tempId = tempid.scrapeFrom(key);
			const realId = resolvedIds[tempId];
			if (tempId && realId) {
				const newKey = key.replace(tempId, realId);
				storage.move(key, newKey);
			}
		});
		// Replace in cached metadata
		const dataKeys = storage.keys().filter(isItemDataKey);
		dataKeys.forEach(key => {
			const data = storage.get(key);
			const realId = resolvedIds[data.id];
			if (realId) {
				data.id = realId;
				storage.set(key, data);
			}
		});
	}

	//======================================================================
	//			Contents

	/**
	 * Returns the current contents (according to the cache) of the file with the
	 * given ID.
	 * @param  {String}  id
	 *         		The ID of the file.
	 * @param  {Boolean} keepSeparate=false
	 *         		`true` if the cached changes should not be applied, but returned
	 *         		separately instead.
	 * @param  {Boolean} mightBeMutated=true
	 *         		`true` if the returned contents might be mutated later on.
	 *         		Ignored if keepSeparate == true.
	 * @return {Formatted contents}
	 *         		The file contents, or {baseContents, changes} if keepSeparate
	 *         		== true.
	 */
	getCachedContents(id, {keepSeparate=false, mightBeMutated=true}={}) {

		// Utility function for cloning before return (if needed):
		const conditionalClone = c =>
				mightBeMutated ? this._contentsHelper.cloneIfNecessary(c) : c;

		// Returned cached pre-patched contents if possible:
		if (!keepSeparate) {
			const memoryCachedContents = this._memoryCache[id];
			if (memoryCachedContents) {
				return conditionalClone(memoryCachedContents);
			}
		}

		const cacheKey = getItemContentsKey(id);
		const rawContents = storage.get(cacheKey);
		// Return empty if no contents:
		if (!rawContents) return rawContents;

		// Convert from raw JSON
		const contents = this._contentsHelper.fromJson(rawContents);

		const patch = this.getCachedPatch(id);

		if (keepSeparate) {
			// Return base contents and changes separately
			return {
				baseContents: contents,
				changes: patch,
			};
		}

		// Apply patch, if there is one:
		const patchedContents = patch
				? this._contentsHelper.patch(patch, contents)
				: contents;

		// Cache in memory the retrieved and patched contents:
		this._memoryCache[id] = patchedContents;

		console.log("%cCached contents retrieved:", "color:chartreuse", patchedContents);

		return conditionalClone(patchedContents);
	}

	getCachedPatch(id) {
		return storage.get(getItemPatchKey(id)) || null;
	}

	cacheContents(id, contents, overwrite=false) {
		const cacheKey = getItemContentsKey(id);
		// Prevent overwriting existing cached data, unless overwrite == true
		if (!overwrite && storage.get(cacheKey)) {
			const err = new Error("Cannot overwrite cached contents without overwrite flag.");
			err.cacheNotEmpty = true;
			throw err;
		}
		const rawContents = this._contentsHelper.toJson(contents);
		storage.set(cacheKey, rawContents);
		console.log("%cContents cached:", "color:chartreuse", rawContents);

		// Cache in memory the new contents:
		this._memoryCache[id] = this._contentsHelper.cloneIfNecessary(contents);

		// Clear cached patch:
		storage.remove(getItemPatchKey(id));
	}

	updateCachedContents(id, newContents) {
		var currentContents = this
				.getCachedContents(id, {mightBeMutated:false, keepSeparate:true})
				.baseContents;

		if (!currentContents) {
			console.warn(
				"Trying to update contents but no cached contents found. " +
				"Are you calling write() on a File before setDefaultContents()?");
			// Write an empty-object placeholder directly to cache, then compute patch.
			// TODO: throw error here instead? This assumes the structure is based on a JSON tree
			currentContents = this._contentsHelper.fromJson({});
			this.cacheContents(id, currentContents);
		}

		const patch = this._contentsHelper.diff(currentContents, newContents);

		// Cache patch:
		if (patch) {
			storage.set(getItemPatchKey(id), patch);
		} else {
			// No changes from base
			storage.remove(getItemPatchKey(id));
		}

		// Cache in memory the updated contents:
		this._memoryCache[id] = this._contentsHelper.cloneIfNecessary(newContents);
	}

	//======================================================================
	//			Sync

	/**
	 * Returns all the ids of files whose contents have been cached.
	 * @return {Array<String>} The file IDs.
	 */
	getAllCachedContentFileIds() {
		const allKeys = storage.keys();

		const patchKeys = allKeys.filter(isContentsKey);
		const ids = patchKeys.map(getIdFromContentsKey);

		return ids;
	}

}

function getItemDataKey(parentId, name) {
	return `metadata-${parentId}/${name}`;
}

function isItemDataKey(key) {
	return key.startsWith("metadata-");
}

const TEMP_ITEM_RECORDS_KEY = "temp_files";

function getItemContentsKey(id) {
	return `contents-${id}`;
}

function getItemPatchKey(id) {
	return `contentpatch-${id}`;
}

function isContentsKey(key) {
	return key.startsWith("contents-");
}

function getIdFromContentsKey(key) {
	return key.match(/^contents-(.*)/)[1];
}

// Track age of contents keys, to allow pruning of old unused contents
storage.setPruningFilter(/^contents-/);
