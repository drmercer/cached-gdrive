
import gds from "./index.js";
import shortid from "shortid";

// Set status change listener
gds.watchConnection(handleConnectionStatusChanged);

// Initialize client
gds.init({
	clientId: "262230741042-6pk925ik2nrmpkp7ekb35r149p1om5t9.apps.googleusercontent.com",
	scopes: [
		"https://www.googleapis.com/auth/drive.appdata",
		"https://www.googleapis.com/auth/drive",
	],
});

// Handle connection status change:
function handleConnectionStatusChanged(state) {
	console.log("%cconnection status: ", "color:cyan", state);
	console.info(
		state.isConnected ? "Connected" : "Not connected",
		" - ",
		state.isSignedIn ? "Signed in" : "Not signed in");
	const btn = document.getElementById("signIn");
	const syncBtn = document.getElementById("sync");
	const content = document.getElementById("content");
	edit = document.getElementById("edit");
	const editLabel = document.getElementById("editlabel");

	syncBtn.onclick = () => gds.requestSync();

	const makeFileBtn = document.getElementById("makeFile");
	makeFileBtn.onclick = function() {
		const name = shortid.generate();
		gds.getAppDataFolder()
			.getFile(name)
			.then(file => file
				.setDefaultContents({})
				.write({ iLikeToEat: "bagels! " + name}))
			.then(() => {
				content.innerText += " - Created file: " + name;
				console.log("Created file " + name);
			});
	};

	const incrementBagelsBtn = document.getElementById("incrementBagels");

	editLabel.innerText = "";
	syncBtn.disabled = true;
	edit.disabled = true;

	if (!state.isSignedIn) {

		btn.onclick = gds.signIn;
		btn.innerText = "Sign In";
		content.innerText = "You are not signed in.";
		syncBtn.disabled = true;

		if (myfile) myfile.unwatch();
		myfile = null;
		incrementBagelsBtn.onclick = null;
		edit.disabled = true;

	} else {
		const user = gds.getCachedUserProfile();
		content.innerText = `Signed in as ${user.name} (${user.email}).`;

		// "Sign out" button
		btn.onclick = () => gds.signOut({promptForForce:true});
		btn.innerText = "Sign Out";

		if (!state.isConnected) {
			content.innerText = "Signed in, but not connected.";
			return;
		}

		if (state.needsToAuthenticate) {
			btn.innerText = "Authenticate";
			btn.onclick = gds.signIn;
			content.innerText = "Signed in, but you need to authenticate with Google first.";
			return;
		}

		syncBtn.disabled = false;
		edit.disabled = false;
		editLabel.innerHTML = "myfile.json's <code>text</code>:";

		const appdata = gds.getAppDataFolder();

		console.groupCollapsed("Testing");

		appdata.listFiles({getAll: true}).then(files => {
			console.log("Files in the appData folder:", files);
		});

		if (!myfile) {
			// Get a file by name (creating it if it doesn't exist), and
			// write some JSON to it:
			var p1 = appdata.getFile("myfile.json")
			.then(file => {// Read from file:
				myfile = file;
				file.setDefaultContents({thisIsSome:"json"});
				return file.watch(handleMyfileContentsChange);
			}).then(() => {
				incrementBagelsBtn.onclick = handleIncrementBagelsClicked;
			});
		}

		edit.onchange = handleTextAreaEdited;

		Promise.all([p1]).then(() => {
			console.groupEnd();
			console.log("%cDone", "color:green");
		}, err => {
			console.groupEnd();
			console.error("Error:", err);
		});
	}
}

var fileContents = null;
var myfile = null;
var edit = null;
function handleMyfileContentsChange(newContents) {
	console.log("Contents changed!", newContents);
	fileContents = newContents;
	edit.value = newContents.text || "";
}

function handleIncrementBagelsClicked() {
	fileContents.bagel = (fileContents.bagel || 0) + 1;
	myfile.write(fileContents);
}

function handleTextAreaEdited(e) {
	fileContents.text = e.currentTarget.value;
	myfile.write(fileContents);
}
