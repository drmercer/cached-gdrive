/*
 * Entry point for users *not* using Immutable.JS
 */
import coreFunc from "./core/core";
import standardFormatParams from "./format/standard.js";

export default coreFunc(standardFormatParams);
