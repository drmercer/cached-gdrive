import jsonMergePatch from "json-merge-patch";

export default {
	diff,
	patch,
	mergeContents,
};

function _clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

function diff(before, after) {
	const changes = jsonMergePatch.generate(before, after);
	return (changes && Object.keys(changes).length) ? changes : null;
}

function patch (before, changes) {
	return jsonMergePatch.apply(_clone(before), changes);
}

function mergeContents(serverContents, cachedBaseContents, localChanges) {
	var serverHadChanges, serverChanges;
	if (serverContents) {
		serverChanges = diff(cachedBaseContents, serverContents);
		serverHadChanges = !!serverChanges;
	} else {
		serverChanges = null;
		serverHadChanges = false;
	}

	// If no server contents or no changes, apply changes to cached base.
	const mergedBaseContents = serverHadChanges ? serverContents : cachedBaseContents;

	const localChangesApplied = localChanges
			? patch(mergedBaseContents, localChanges)
			: mergedBaseContents;

	var mergedContents;
	if (serverChanges) {
		// Apply server changes again, to make sure they take priority over the local changes:
		mergedContents = patch(localChangesApplied, serverChanges);
	} else {
		mergedContents = localChangesApplied;
	}

	return {
		mergedContents,
		serverHadChanges,
	};
}
