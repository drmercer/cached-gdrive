/**
 * The defaults of the parameters passed into the core function exported by core.js
 */
export default {
	willMutateOutput: true,

	toJson(notraw) {
		return notraw;
	},

	fromJson(raw) {
		return raw;
	},

	diff() {
		throw new Error("Must specify a diff(before, after) function when initializing cached-gdrive");
	},

	patch() {
		throw new Error("Must specify a patch(before, changes) function when initializing cached-gdrive");
	},
};
