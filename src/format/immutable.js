import Immutable from "immutable";
import imp from "immutable-merge-patch";

export default {
	// convert just-read JSON to an Immutable object
	fromJson: (raw) => Immutable.fromJS(raw),
	// convert an Immutable object into plain JSON to be written
	toJson: (notraw) => notraw.toJS(),
	// tell cached-gdrive not to store a clone of the read data, since mutation is not a concern
	willMutateOutput: false,

	diff,
	patch,
	mergeContents,
};

// Compute an Immutable diff, then convert to plain JS.
function diff(before, after) {
	const changes = imp.generate(before, after);
	// Return null if nothing in `changes`:
	return !changes.isEmpty() ? changes.toJS() : null;
}

// Convert the patch from plain JS to Immutable, then apply the patch.
function patch(before, changes) {
	return imp.apply(before, Immutable.fromJS(changes));
}

function mergeContents(serverContents, cachedBaseContents, localChanges) {
	var serverHadChanges, serverChanges;
	if (serverContents) {
		serverChanges = diff(cachedBaseContents, serverContents);
		serverHadChanges = !!serverChanges;
	} else {
		serverChanges = null;
		serverHadChanges = false;
	}

	// If no server contents or no changes, apply changes to cached base.
	const mergedBaseContents = serverHadChanges ? serverContents : cachedBaseContents;

	const localChangesApplied = localChanges
			? patch(mergedBaseContents, localChanges)
			: mergedBaseContents;

	var mergedContents;
	if (serverChanges) {
		// Apply server changes again, to make sure they take priority over the local changes:
		mergedContents = patch(localChangesApplied, serverChanges);
	} else {
		mergedContents = localChangesApplied;
	}

	return {
		mergedContents,
		serverHadChanges,
	};
}
