/*
 * Entry point for users desiring to store Immutable.JS data structures
 */

import coreFunc from "./core/core";
import immutableParams from "./format/immutable";

export default coreFunc(immutableParams);
