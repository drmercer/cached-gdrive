# cached-gdrive

[![npm version](https://badge.fury.io/js/cached-gdrive.svg)](https://badge.fury.io/js/cached-gdrive)

TODO

# Limitations
- **cached-gdrive** is currently best suited for use in the app data folder, since
  files there can only be changed by your app. This library doesn't track files
  being moved, so things might get messy if you use it with files the user can
  move.
- When going from offline to online, it handles errors properly (at least, it's
  designed to do so), but I'm pretty sure there's no way to consume network
  errors from JavaScript, which means network failures still get logged in
  debugging consoles. *(See the [Tips](#tips) section for help with hiding those
  errors.)*

# Tips
- When debugging network-change capabilities, you'll probably want to filter out
  Google-API-related network errors from your console. In Chrome Dev Tools, you
  can use this RegEx as a filter to accomplish that:
  `/^(?!.*(?:google|gstatic).*net::ERR)/`
