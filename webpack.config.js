/* eslint-disable */

module.exports = {
	entry: {
		"index": ["babel-polyfill", "./src/index.js"],
		"index-immutable": ["babel-polyfill", "./src/index-immutable.js"],
		"sample": ["babel-polyfill", "./src/sample.js"],
	},
	output: {
		filename: "./dist/[name].js",
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	devtool: "source-map",
};
