/* eslint-disable */

var mainConfig = require("./webpack.config.js");

var nodeExternals = require("webpack-node-externals");

var config = Object.assign(mainConfig, {
	output: {
		filename: "./dist/[name].js",
		library: "cached-gdrive",
		libraryTarget: "umd",
		umdNamedDefine: true,
	},
	externals: [
		nodeExternals(),
	],
});

// Remove sample.js:
delete config.entry.sample;

module.exports = config;
